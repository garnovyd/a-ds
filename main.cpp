#include <iostream>
#include <string>
#include <vector>

using namespace std;
using LL = long long;

std::vector<LL> pref_func(const std::string& s){
    std::vector<LL> pf(s.size() + 1, 0);

    for (int i = 1; i < pf.size(); ++i) {
        LL k = pf[i-1];

        while (k > 0 && s[i] != s[k]){
            k = pf[k - 1];
        }
        if (s[k] == s[i])
            k++;

        pf[i] = k;
    }

    return pf;
}

int main() {
    std::string templ, str;
    cin >> templ >> str;

    std::string buffer = templ + '$' + str;
    std::vector<LL> pf = pref_func(buffer);

    for (int i = 0; i < pf.size(); ++i) {
        if (pf[i] == templ.size())
            cout << i - 2*templ.size() << ' ';
    }

    return 0;
}